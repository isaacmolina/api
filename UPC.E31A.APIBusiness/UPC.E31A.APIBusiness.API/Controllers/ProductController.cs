﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UPC.Business.API.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/product")]
    public class ProductController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly iProductRepository _ProductRepository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ProductRepository"></param>
        public ProductController(iProductRepository ProductRepository)
        {
            _ProductRepository = ProductRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetProducts")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetProducts")]
        public ActionResult GetProducts()
        {
            var ret = _ProductRepository.GetProducts();
            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetProductByParams")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetProductByParams")]
        public ActionResult GetProductByParams(int id_producto, string nombre)
        {
            var ret = _ProductRepository.GetProductByParams(id_producto, nombre);
            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }
        /// <summary>
        /// InsertCroduct
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]

        [AllowAnonymous]
        [HttpPost]
        [Route("InsertProduct")]
        public void InsertProduct(EntityProducto Cate)
        {
            _ProductRepository.InsertProduct(Cate);
        }
        /// <summary>
        /// DeleteProducto
        /// </summary>
        /// <param name="id_producto"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("DeleteProducto")]
        [AllowAnonymous]
        [HttpGet]
        [Route("DeleteProducto")]
        public ActionResult DeleteProducto(int id_producto)
        {

            var ret = _ProductRepository.DeleteProducto(id_producto);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }
        /// <summary>
        /// UpdateProducto
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("UpdateProducto")]
        [AllowAnonymous]
        [HttpGet]
        [Route("UpdateProducto")]
        public ActionResult UpdatePorducto(int id_producto, string codigo, int orden, int estado, string url, string nombre, string resumen, string descripcion, string imagen, int destacado, int oferta, decimal precio, int porcentaje, decimal precio_descuento, int id_categoria)
        {

            var ret = _ProductRepository.UpdateProducto(id_producto, codigo, orden, estado, url, nombre, resumen, descripcion, imagen, destacado, oferta, precio, porcentaje, precio_descuento, id_categoria);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("FeaturedProducts")]
        [AllowAnonymous]
        [HttpGet]
        [Route("FeaturedProducts")]
        public ActionResult FeaturedProducts()
        {
            var ret = _ProductRepository.FeaturedProducts();
            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }




    }
}
