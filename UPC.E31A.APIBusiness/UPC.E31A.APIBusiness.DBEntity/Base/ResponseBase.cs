﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class ResponseBase
    {
        public bool isSuccess { get; set; }
        public string message { get; set; }
        public string codeError { get; set; }
        public string messageError { get; set; }
        public Object data { get; set; }
    }
}