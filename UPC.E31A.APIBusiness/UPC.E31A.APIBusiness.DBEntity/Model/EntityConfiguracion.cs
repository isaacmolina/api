﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityConfiguracion : EntityBase
    {
        public List<Options> options;
        public int id { get; set; }
        public string llave { get; set; }
        public string valor { get; set; }
        public string descripcion { get; set; }
        public int tipo { get; set; }
        public string visible { get; set; }

    }
}
