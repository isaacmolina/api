﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityCategoria
    {
		public int Id_Categoria { get; set; }
		public string Nombre { get; set; }
		public string Imagen { get; set; }
		public int Orden { get; set; }
		public int Estado { get; set; }
	}
}
