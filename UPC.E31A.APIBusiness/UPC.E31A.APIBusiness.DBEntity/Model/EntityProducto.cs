﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityProducto : EntityBase
    {
        public int id_producto { get; set; }
        public string codigo { get; set; }
        public int orden { get; set; }
        public int estado { get; set; }
        public string url { get; set; }
        public string nombre { get; set; }
        public string resumen { get; set; }
        public string descripcion { get; set; }
        public string imagen { get; set; }
        public int destacado { get; set; }
        public int oferta { get; set; }
        public decimal precio { get; set; }
        public int porcentaje { get; set; }
        public float precio_descuento { get; set; }
        public int id_categoría { get; set; }
    }
}
