﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface ICategoriaRepository
    {
        ResponseBase GetCategorias();
        void InsertCategoria(EntityCategoria Cate);
        ResponseBase DeleteCategoria(int id_categoria);
        ResponseBase GetCatbyId(int id_categoria);
        ResponseBase UpdateCategoria(int id_categoria, string nombre, string imagen, int orden, int estado);

    }
}
