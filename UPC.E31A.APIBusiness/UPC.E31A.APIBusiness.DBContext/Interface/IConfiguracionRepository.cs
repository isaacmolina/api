﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface IConfiguracionRepository
    {
        List<EntityConfiguracion> GetConfiguraciones();
        object GetConfiguracionByLlave(string llave);
        /*EntityConfiguracion GetConfiguracionByLlave(string llave);
EntityConfiguracion GetConfiguracionById(int id);
void InsertConfiguracion(EntityConfiguracion Configuracion);
EntityConfiguracion DeleteUsuario(int id);
*/
        //EntityConfiguracion UpdateUsuario(int id, string password);

    }
}
