﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext

{
    public class CategoriaRepository: BaseRepository, ICategoriaRepository
    {
        public ResponseBase GetCategorias()
        {
            ResponseBase responsebase = new ResponseBase();
            var returnEntity = new List<EntityCategoria>();

            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"sp_list_categorias";
                    returnEntity = db.Query<EntityCategoria>(sql, commandType: CommandType.StoredProcedure).ToList();
                }
            
                responsebase.isSuccess = true;
                responsebase.codeError = "000";
                responsebase.messageError = string.Empty;
                responsebase.message = "Listado de categorias";
                responsebase.data = returnEntity;
        }
            catch (Exception ex)
            {
                responsebase.isSuccess = false;
                responsebase.codeError = "999";
                responsebase.messageError = ex.Message;
                responsebase.message = String.Empty;
                responsebase.data = returnEntity;
            }

            return responsebase;
        }

        public void InsertCategoria(EntityCategoria Cate)
        {
           
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@IDCAT", value: Cate.Id_Categoria, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add(name: "@NOMBRE", value: Cate.Nombre, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@IMAGEN", value: Cate.Imagen, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@ORDEN", value: Cate.Orden, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@ESTADO", value: Cate.Estado, dbType: DbType.String, direction: ParameterDirection.Input);

                    const string sql = @"sp_InsertarCategoria";

                    db.Query<EntityCategoria>(sql, param: p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
                

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ResponseBase DeleteCategoria(int id_categoria)
        {
            ResponseBase responsebase = new ResponseBase();
            var returnEntity = new EntityCategoria();
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@IDCATEGORIA", value: id_categoria, dbType: DbType.Int32, direction: ParameterDirection.Input);

                    const string sql = @"sp_EliminarCategoriaPorID";

                    returnEntity = db.Query<EntityCategoria>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
                responsebase.isSuccess = true;
                responsebase.codeError = "000";
                responsebase.messageError = string.Empty;
                responsebase.message = "Eliminacion correcta";
                responsebase.data = returnEntity;
            }
            catch (Exception ex)
            {
                responsebase.isSuccess = false;
                responsebase.codeError = "999";
                responsebase.messageError = ex.Message;
                responsebase.message = String.Empty;
                responsebase.data = returnEntity;
            }

            return responsebase;
        }

        //msalas 02052020 ResponseBase GetCatbyId(int id_categoria);
        public ResponseBase GetCatbyId(int id_categoria)
        {
            ResponseBase responsebase = new ResponseBase();
            var returnEntity = new EntityCategoria();


            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@IDCATE", value: id_categoria, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    
                    const string sql = @"sp_categorias_by_id";
                    returnEntity = db.Query<EntityCategoria>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }

                responsebase.isSuccess = true;
                responsebase.codeError = "000";
                responsebase.messageError = string.Empty;
                responsebase.message = "Lista de datos por id";
                responsebase.data = returnEntity;
            }
            catch (Exception ex)
            {
                responsebase.isSuccess = false;
                responsebase.codeError = "999";
                responsebase.messageError = ex.Message;
                responsebase.message = String.Empty;
                responsebase.data = returnEntity;
            }

            return responsebase;
        }


        public ResponseBase UpdateCategoria(int id_categoria, string nombre, string imagen, int orden, int estado)
        {
            ResponseBase responsebase = new ResponseBase();
            var returnEntity = new EntityCategoria();
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@IDCAT", value: id_categoria, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add(name: "@NOMBRE", value: nombre, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@IMAGEN", value: imagen, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@ORDEN", value: orden, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add(name: "@ESTADO", value: estado, dbType: DbType.Int32, direction: ParameterDirection.Input);

                    const string sql = @"sp_UpdateCategoriaporID";

                    returnEntity = db.Query<EntityCategoria>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
                responsebase.isSuccess = true;
                responsebase.codeError = "000";
                responsebase.messageError = string.Empty;
                responsebase.message = "Actualización correcta";
                responsebase.data = returnEntity;
            }
            catch (Exception ex)
            {
                responsebase.isSuccess = false;
                responsebase.codeError = "999";
                responsebase.messageError = ex.Message;
                responsebase.message = String.Empty;
                responsebase.data = returnEntity;
            }

            return responsebase;
        }

    }

   
}
